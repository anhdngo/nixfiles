{
  ".profile".source = ./dotfiles/profile;
  ".zshrc".source = ./dotfiles/zshrc;
  ".aliases".source = ./dotfiles/aliases;
  ".vimrc".source = ./dotfiles/vimrc;
  ".gitconfig".source = ./dotfiles/gitconfig;
  ".Xresources".source = ./dotfiles/Xresources;
  ".Xdefaults".source = ./dotfiles/Xresources;

  ".config/i3/config".source = ./dotfiles/config/i3/config;
  ".config/rofi/config.rasi".source = ./dotfiles/config/rofi/config.rasi;
  ".config/rofi/Arc-Dark.rasi".source = ./dotfiles/config/rofi/Arc-Dark.rasi;
  ".config/Thunar/uca.xml".source = ./dotfiles/config/Thunar/uca.xml;
  ".config/mimeapps.list".source = ./dotfiles/config/mimeapps.list;
  ".config/gtk-3.0/bookmarks".source = ./dotfiles/config/gtk-3.0/bookmarks;
}
