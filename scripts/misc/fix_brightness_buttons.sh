sudo tee <<'EOF' /etc/X11/xorg.conf 1> /dev/null
Section "Device"
    Identifier  "Intel Graphics" 
    Driver      "intel"
    Option      "Backlight"  "intel_backlight"
EndSection

EOF
