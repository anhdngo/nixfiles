pkgs: with pkgs; [
  # Essential
  git
  pavucontrol
  rofi
  scrot
  kitty
  p7zip
  picom
  xclip
  imagemagick
  xorg.xrandr

  # Other
  feh
  powerline-fonts
  arandr
  ffmpegthumbnailer
  xarchiver
  neofetch
  xorg.xbacklight
  guake

  # Applications
  brave
  vscode
  obsidian
  vlc
  qbittorrent
  cryptomator
  veracrypt
  pcloud
  nomacs
  gimp
  youtube-dl
  keepassxc
  discord
  #retroarch

  # Fonts
  (pkgs.nerdfonts.override { fonts = [ "DejaVuSansMono" "DroidSansMono" "Hack" ]; })

  # Themes
  # arc-theme # doesn't work here, install with dnf
  # lxappearance
]
